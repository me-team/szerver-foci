package hu.unimiskolc.iit.foci.client.model;

import lombok.Data;

@Data
public class Movement {
    private String playerId;
    private int direction;
    private int distance;
}
