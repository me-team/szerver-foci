package hu.unimiskolc.iit.foci.client.model;

import lombok.Data;

@Data
public class Shot {
    private String playerId;
    private int direction;
    private int distance;
}
