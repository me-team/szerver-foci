package hu.unimiskolc.iit.foci.client.model;

public enum MessageTypes {
    CONNECT, DISCONNECT, MOVE, SHOT, GETSTATUS, CONNECTED, WAITING, DISCONNECTED, STATUS, ERROR
}
