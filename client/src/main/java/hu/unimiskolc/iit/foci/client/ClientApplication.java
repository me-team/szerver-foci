package hu.unimiskolc.iit.foci.client;

import hu.unimiskolc.iit.foci.client.controller.ClientController;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class ClientApplication {

    public static void main(String[] args) {
        // SpringApplication.run(ClientApplication.class, args);
        ClientController stompClient = new ClientController();
        stompClient.run();
        new Scanner(System.in).nextLine(); // Don't close immediately.

    }

}
