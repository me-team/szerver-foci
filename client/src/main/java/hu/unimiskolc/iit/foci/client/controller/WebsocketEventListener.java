package hu.unimiskolc.iit.foci.client.controller;

import hu.unimiskolc.iit.foci.client.model.MessageTypes;
import hu.unimiskolc.iit.foci.client.model.messages.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import javax.websocket.Session;

@Component
public class WebsocketEventListener {

    private static Logger log = LoggerFactory.getLogger(WebsocketEventListener.class);

    @Autowired
    private SimpMessageSendingOperations sendingOperations;
/*
    @EventListener
    public void HandleWebsocketConnectListener(final SessionConnectedEvent connectedEvent) {

        log.info("Logged In");

    }

    @EventListener
    public void HandleWebsocketDisconnectListener(final SessionDisconnectEvent connectedEvent) {
        final StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(connectedEvent.getMessage());
        final String discId = (String) headerAccessor.getSessionAttributes().get("userId");
        Message message = Message.builder()
                .messageType(MessageTypes.DISCONNECTED)
                .clientId(discId).build();
        log.info("Hello Bello");
        sendingOperations.convertAndSend("/topic/public");
    }
*/
}