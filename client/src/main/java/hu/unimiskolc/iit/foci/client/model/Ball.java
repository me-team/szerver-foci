package hu.unimiskolc.iit.foci.client.model;

import lombok.Data;

@Data
public class Ball {
    private Position pos;
}
