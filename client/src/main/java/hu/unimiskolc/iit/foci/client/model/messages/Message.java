package hu.unimiskolc.iit.foci.client.model.messages;


import hu.unimiskolc.iit.foci.client.model.*;
import lombok.*;

import java.security.GeneralSecurityException;
import java.util.List;
import java.util.UUID;

//@NoArgsConstructor
@ToString
@RequiredArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class Message {
    @Getter
    private MessageTypes messageType;
    @Getter
    private UUID clientId;
    @Getter
    private UUID id;
    @Getter
    private UUID gameStateId;
    @Getter
    private List<Player> lineUp;
    @Getter
    private List<Movement> movements;
    @Getter
    private Shot shot;
    @Getter
    private String Error;
    @Getter
    private FootballGame game;
}
