package hu.unimiskolc.iit.foci.client.model;

import lombok.Data;

@Data
public class Player {
    private String id;
    private Position pos;
}
