package hu.unimiskolc.iit.foci.client.controller;

import hu.unimiskolc.iit.foci.client.model.MessageTypes;
import hu.unimiskolc.iit.foci.client.model.messages.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;

import java.lang.reflect.Type;
import java.util.UUID;

public class MyStompSessionHandler extends StompSessionHandlerAdapter {
    private final UUID ownClientUUID;
    private UUID gameStateId = null;
    private final Logger log = LogManager.getLogger(MyStompSessionHandler.class);

    public MyStompSessionHandler(UUID ownClientUUID) {
        this.ownClientUUID = ownClientUUID;
    }

    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        log.info("New session established : " + session.getSessionId());
        session.subscribe("/topic/game", this);
        log.info("Subscribed to /topic/game");
        session.send("/app/login", getSampleMessage());
        log.info("Message sent to websocket server");
    }

    @Override
    public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
        log.error("Got an exception", exception);
    }

    @Override
    public Type getPayloadType(StompHeaders headers) {
        return Message.class;
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
        Message msg = (Message) payload;
        if (msg.getClientId().equals(ownClientUUID) || msg.getGameStateId().equals(gameStateId))
            switch (msg.getMessageType()) {
                case WAITING: {
                    log.info("Waiting");
                    gameStateId = msg.getGameStateId();
                }
                break;
                case CONNECTED:
                    log.info("Connected");
                    gameStateId = msg.getGameStateId();
                    break;
            }


        //Message msg;
        //Message.MessageBuilder builder= Message.builder();
        //log.info("Message recieved: " + msg.toString());

        //log.info(msg.getMessageType().toString());
        //log.info("Received : " + msg.getText() + " from : " + msg.getFrom());
    }

    /**
     * A sample message instance.
     *
     * @return instance of <code>Message</code>
     */
    private Message getSampleMessage() {
        UUID uuid = UUID.randomUUID();
        Message msg = Message.builder().messageType(MessageTypes.CONNECT).clientId(ownClientUUID).
                build();
        log.info("Message sent: " + msg.toString());
        // msg.setFrom("Nicky");
        //msg.setText("Howdy!!");
        return msg;
    }
}

