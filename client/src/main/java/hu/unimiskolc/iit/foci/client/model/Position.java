package hu.unimiskolc.iit.foci.client.model;

import lombok.Data;

@Data
public class Position {
    private int x, y;
}
