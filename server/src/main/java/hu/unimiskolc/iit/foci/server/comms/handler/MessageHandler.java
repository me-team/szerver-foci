package hu.unimiskolc.iit.foci.server.comms.handler;

import hu.unimiskolc.iit.foci.server.comms.messages.*;
import hu.unimiskolc.iit.foci.server.engine.IGameLogic;
import hu.unimiskolc.iit.foci.server.model.Client;
import hu.unimiskolc.iit.foci.server.model.GameState;
import hu.unimiskolc.iit.foci.server.model.MessageTypes;
import hu.unimiskolc.iit.foci.server.model.Session;
import hu.unimiskolc.iit.foci.server.persist.dao.IDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class MessageHandler implements IMessageHandler {

    private IGameLogic gameLogic;

    private IDao<Client> clientDao;
    private IDao<Session> sessionDao;
    private IDao<GameState> gameStateDao;

    @Autowired
    public void setClientDao(IDao<Client> clientDao) {
        this.clientDao = clientDao;
    }

    @Autowired
    public void setSessionDao(IDao<Session> sessionDao) {
        this.sessionDao = sessionDao;
    }

    @Autowired
    public void setGameStateDao(IDao<GameState> gameStateDao) {
        this.gameStateDao = gameStateDao;
    }

    @Autowired
    public void setGameLogic(IGameLogic gameLogic) {
        this.gameLogic = gameLogic;
    }

    @Override
    public LoginOutputMessage handleLogin(final LoginMessage message) {
        // TODO: put clients in DB, clientDao.Add()
        if (clientDao.getFirst(c -> c.getId().equals(message.getClientId())) == null) {
            clientDao.add(new Client(message.getClientId()));
        } else {
            // client already logged in, no response required.
            return null;
        }

        Session clientSession = sessionDao.getFirst(Session::isOpenToJoin);
        var lomBuilder = LoginOutputMessage.builder();
        // create session
        if (clientSession == null) {
            GameState gameState = new GameState(UUID.randomUUID());
            gameStateDao.add(gameState);
            clientSession = new Session()
                    .setId(UUID.randomUUID())
                    .setGameStateId(gameState.getId())
                    .setClient1Id(message.getClientId());
            sessionDao.add(clientSession);
            lomBuilder.gameStateId(clientSession.getGameStateId())
                    .clientId(message.getClientId())
                    .id(clientSession.getId())
                    .messageType(MessageTypes.WAITING);


        } else { // put client in session as player2
            clientSession.setClient2Id(message.getClientId());
            sessionDao.update(clientSession);
            // TODO: broadcast to client1 with his clientID
            lomBuilder.gameStateId(clientSession.getGameStateId())
                    .clientId(message.getClientId())
                    .id(clientSession.getId())
                    .messageType(MessageTypes.CONNECTED);
        }
        return lomBuilder.build();
    }

    @Override
    public GameOutputMessage handleGame(final GameMessage message) {
        Session session = sessionDao.getFirst(s -> message.getClientId().equals(s.getClient1Id()) || message.getClientId().equals(s.getClient2Id()));
        if (session == null) {
            // fatal error, session should exist because of handelLogin
            throw new NullPointerException("session is null, message received: " + message.toString());
        }

        OutputMessage originalOutput = session.getOutputMessage();
        // Client response is in fact a response to the latest message sent.
        if (originalOutput != null && message.getId().equals(originalOutput.getId())) {
            saveClientMessage(message, session);
        }

        if (session.bothClientsHaveInputs()) {
            // handle both clients input (pass it to gameLogic)
            gameLogic.playGame(session.getGameStateId());
            // TODO: build output message from new gameState, (gameLogic should change gameState within playGame)
            return buildAndSaveGameOutputMessage(session);
        }

        // else
        return null;
    }

    private GameOutputMessage buildAndSaveGameOutputMessage(final Session session) {
        GameState gameState = gameStateDao.getFirst(gs -> gs.getId().equals(session.getGameStateId()));
        var messageBuilder = GameOutputMessage.builder();
        // todo: build
        GameOutputMessage gameOutputMessage = messageBuilder
                .id(UUID.randomUUID())
                .gameStateId(gameState.getId())
                .build();
        // put new output message into session, null client responses
        session
                .setOutputMessage(gameOutputMessage)
                .setClient1response(null)
                .setClient2response(null);
        return gameOutputMessage;
    }

    private void saveClientMessage(final GameMessage message, final Session session) {
        if (message.getClientId().equals(session.getClient1Id())) {
            session.setClient1response(message);
        } else if (message.getClientId().equals(session.getClient2Id())) {
            session.setClient2response(message);
        } else {
            throw new IllegalArgumentException("Client disappeared from session. Message: " + message.toString() + ", Session: " + session.toString());
        }
    }
}
