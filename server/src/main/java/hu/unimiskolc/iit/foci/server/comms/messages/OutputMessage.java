package hu.unimiskolc.iit.foci.server.comms.messages;

import hu.unimiskolc.iit.foci.server.model.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import java.util.List;
import java.util.UUID;

@Data
@Getter
@AllArgsConstructor
public class OutputMessage {
    private UUID id;
    private MessageTypes messageType;
    private UUID clientId;
    private UUID gameStateId;
    private List<Player> lineUp;
    private List<Movement> movements;
    private Shot shot;
    private String Error;
    private FootballGame game;
}
