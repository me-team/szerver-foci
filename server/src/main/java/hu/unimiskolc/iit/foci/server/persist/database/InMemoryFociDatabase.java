package hu.unimiskolc.iit.foci.server.persist.database;

import hu.unimiskolc.iit.foci.server.model.Client;
import hu.unimiskolc.iit.foci.server.model.GameState;
import hu.unimiskolc.iit.foci.server.model.Session;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class InMemoryFociDatabase implements IFociDatabase {

    private Map<UUID, Client> clients;
    private Map<UUID, GameState> gameStates;
    private Map<UUID, Session> sessions;

    public InMemoryFociDatabase() {
        this.clients = new HashMap<>(2);
        this.gameStates = new HashMap<>(1);
        this.sessions = new HashMap<>(1);
    }

    @Override
    public void addClient(Client model) {
        clients.put(model.getId(), model);
    }

    @Override
    public void updateClient(Client model) {
        clients.replace(model.getId(), model);
    }

    @Override
    public void deleteClient(UUID id) {
        clients.remove(id);
    }

    @Override
    public Client getClient(UUID id) {
        return clients.get(id);
    }

    @Override
    public Client getFirstClient(Predicate<? super Client> predicate) {
        return clients.values().stream().filter(predicate).findFirst().orElse(null);
    }

    @Override
    public List<Client> getAllClients() {
        return new ArrayList<>(clients.values());
    }

    @Override
    public List<Client> getAllClientsWithFilter(Predicate<? super Client> predicate) {
        return clients.values().stream().filter(predicate).collect(Collectors.toList());
    }

    @Override
    public void addGameState(GameState model) {
        gameStates.put(model.getId(), model);
    }

    @Override
    public void updateGameState(GameState model) {
        gameStates.replace(model.getId(), model);
    }

    @Override
    public void deleteGameState(UUID id) {
        gameStates.remove(id);
    }

    @Override
    public GameState getGameState(UUID id) {
        return gameStates.get(id);
    }

    @Override
    public GameState getFirstGameState(Predicate<? super GameState> predicate) {
        return gameStates.values().stream().filter(predicate).findFirst().orElse(null);
    }

    @Override
    public List<GameState> getAllGameStates() {
        return new ArrayList<>(gameStates.values());
    }

    @Override
    public List<GameState> getAllGameStatesWithFilter(Predicate<? super GameState> predicate) {
        return gameStates.values().stream().filter(predicate).collect(Collectors.toList());
    }

    @Override
    public void addSession(Session model) {
        sessions.put(model.getId(), model);
    }

    @Override
    public void updateSession(Session model) {
        sessions.replace(model.getId(), model);
    }

    @Override
    public void deleteSession(UUID id) {
        sessions.remove(id);
    }

    @Override
    public Session getSession(UUID id) {
        return sessions.get(id);
    }

    @Override
    public Session getFirstSession(Predicate<? super Session> predicate) {
        return sessions.values().stream().filter(predicate).findFirst().orElse(null);
    }

    @Override
    public List<Session> getAllSessions() {
        return new ArrayList<>(sessions.values());
    }

    @Override
    public List<Session> getAllSessionsWithFilter(Predicate<? super Session> predicate) {
        return sessions.values().stream().filter(predicate).collect(Collectors.toList());
    }
}
