package hu.unimiskolc.iit.foci.server.model;

import lombok.Data;

import java.util.List;

@Data
public class Team {
    private List<Player> players;
}
