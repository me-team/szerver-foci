package hu.unimiskolc.iit.foci.server.game;

import hu.unimiskolc.iit.foci.server.engine.IGameLogic;
import hu.unimiskolc.iit.foci.server.model.GameState;
import hu.unimiskolc.iit.foci.server.persist.dao.IDao;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
// TODO: uncomment if singleton isn't good practice, and would rather use prototype (new instance on every inject)
//@Scope(value="prototype", proxyMode= ScopedProxyMode.TARGET_CLASS)
@NoArgsConstructor
public class GameLogic implements IGameLogic {

    private IDao<GameState> gameStateDao;

    @Autowired
    public void setGameStateDao(IDao<GameState> gameStateDao) {
        this.gameStateDao = gameStateDao;
    }

    @Override
    public void playGame(UUID gameId) {
        GameState gameState = gameStateDao.get(gameId);
        // TODO: play the game, using both inputs
    }
}
