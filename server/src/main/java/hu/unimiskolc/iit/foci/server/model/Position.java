package hu.unimiskolc.iit.foci.server.model;

import lombok.Data;

@Data
public class Position {
    private int x, y;
}
