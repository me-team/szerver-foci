package hu.unimiskolc.iit.foci.server.game;

import hu.unimiskolc.iit.foci.server.engine.IGameEngine;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@NoArgsConstructor
@AllArgsConstructor
@Component
@Profile("!test")
public class ServerRunner implements CommandLineRunner {

    private IGameEngine gameEngine;

    @Autowired
    public void setGameEngine(IGameEngine gameEngine) {
        this.gameEngine = gameEngine;
    }

    @Override
    public void run(String... args) throws Exception {
        //gameEngine.run();
    }
}
