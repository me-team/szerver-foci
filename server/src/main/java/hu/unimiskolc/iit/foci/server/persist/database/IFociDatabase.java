package hu.unimiskolc.iit.foci.server.persist.database;

public interface IFociDatabase extends IClientDatabase, IGameStateDatabase, ISessionDatabase {

}
