package hu.unimiskolc.iit.foci.server.persist.database;

import hu.unimiskolc.iit.foci.server.model.Session;

import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;

public interface ISessionDatabase {

    void addSession(Session model);

    void updateSession(Session model);

    void deleteSession(UUID id);

    Session getSession(UUID id);

    Session getFirstSession(Predicate<? super Session> predicate);

    List<Session> getAllSessions();

    List<Session> getAllSessionsWithFilter(Predicate<? super Session> predicate);
}
