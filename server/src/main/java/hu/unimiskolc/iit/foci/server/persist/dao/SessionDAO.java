package hu.unimiskolc.iit.foci.server.persist.dao;

import hu.unimiskolc.iit.foci.server.model.Session;
import hu.unimiskolc.iit.foci.server.persist.database.IFociDatabase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;

@Service
public class SessionDAO implements IDao<Session> {

    private IFociDatabase database;

    @Autowired
    public void setDatabase(IFociDatabase database) {
        this.database = database;
    }

    @Override
    public void add(Session session) {
        database.addSession(session);
    }

    @Override
    public void update(Session session) {
        database.updateSession(session);
    }

    @Override
    public void delete(UUID id) {
        database.deleteSession(id);
    }

    @Override
    public Session get(UUID id) {
        return database.getSession(id);
    }

    @Override
    public Session getFirst(Predicate<? super Session> predicate) {
        return database.getFirstSession(predicate);
    }

    @Override
    public List<Session> getAll() {
        return database.getAllSessions();
    }

    @Override
    public List<Session> getAllWithFilter(Predicate<? super Session> predicate) {
        return database.getAllSessionsWithFilter(predicate);
    }
}
