package hu.unimiskolc.iit.foci.server.model;

import java.util.UUID;

public interface IUuid {
    UUID getId();
}
