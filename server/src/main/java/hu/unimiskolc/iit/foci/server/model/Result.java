package hu.unimiskolc.iit.foci.server.model;

import lombok.Data;

@Data
public class Result {
    private int team1;
    private int team2;
}
