package hu.unimiskolc.iit.foci.server.model;

import lombok.Data;

@Data
public class Ball {
    private Position pos;
}
