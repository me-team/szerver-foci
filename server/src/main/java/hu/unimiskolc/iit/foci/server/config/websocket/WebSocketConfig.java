package hu.unimiskolc.iit.foci.server.config.websocket;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    @Value("${websocket.app-destination}")
    private String appDestination;
    @Value("${websocket.simple-broker}")
    private String simpleBroker;
    @Value("${websocket.endpoint.game}")
    private String endPointGame;
    @Value("${websocket.endpoint.login}")
    private String endPointLogin;

    @Override
    public void configureMessageBroker(final MessageBrokerRegistry config) {
        config.enableSimpleBroker(simpleBroker);
        config.setApplicationDestinationPrefixes(appDestination);
    }

    @Override
    public void registerStompEndpoints(final StompEndpointRegistry registry) {
        registry.addEndpoint(endPointGame);
        registry.addEndpoint(endPointGame).withSockJS();
        registry.addEndpoint(endPointLogin);
        registry.addEndpoint(endPointLogin).withSockJS();
    }

}
