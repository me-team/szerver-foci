package hu.unimiskolc.iit.foci.server.persist.dao;

import hu.unimiskolc.iit.foci.server.model.IUuid;

import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;

public interface IDao<TModel extends IUuid> {
    void add(TModel model);

    void update(TModel model);

    void delete(UUID id);

    TModel get(UUID id);

    TModel getFirst(Predicate<? super TModel> predicate);

    List<TModel> getAll();

    List<TModel> getAllWithFilter(Predicate<? super TModel> predicate);
}
