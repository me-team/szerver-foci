package hu.unimiskolc.iit.foci.server.comms.messages;

import hu.unimiskolc.iit.foci.server.model.*;
import lombok.Builder;
import lombok.Getter;

import java.util.List;
import java.util.UUID;

@Getter
public class LoginOutputMessage extends OutputMessage {
    @Builder
    public LoginOutputMessage(UUID id, MessageTypes messageType, UUID clientId, UUID gameStateId, List<Player> lineUp, List<Movement> movements, Shot shot, String Error, FootballGame game) {
        super(id, messageType, clientId, gameStateId, lineUp, movements, shot, Error, game);
    }
}
