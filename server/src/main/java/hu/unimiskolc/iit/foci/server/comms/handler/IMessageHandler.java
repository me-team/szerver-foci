package hu.unimiskolc.iit.foci.server.comms.handler;

import hu.unimiskolc.iit.foci.server.comms.messages.GameMessage;
import hu.unimiskolc.iit.foci.server.comms.messages.GameOutputMessage;
import hu.unimiskolc.iit.foci.server.comms.messages.LoginMessage;
import hu.unimiskolc.iit.foci.server.comms.messages.LoginOutputMessage;

public interface IMessageHandler {
    LoginOutputMessage handleLogin(final LoginMessage message);

    GameOutputMessage handleGame(final GameMessage message);
}
