package hu.unimiskolc.iit.foci.server.model;

import lombok.Data;

import java.util.Date;

@Data
public class FootballGame {
    private String id;
    private Date date;
    private Team team1;
    private Team team2;
    private Result res;
}
