package hu.unimiskolc.iit.foci.server.model;

public enum MessageTypes {
    CONNECT, DISCONNECT, MOVE, SHOT, GETSTATUS, CONNECTED, WAITING, DISCONNECTED, STATUS, ERROR
}
