package hu.unimiskolc.iit.foci.server.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.UUID;

@NoArgsConstructor
//@RequiredArgsConstructor
@AllArgsConstructor
@Data
@Accessors(chain = true)
public class Client implements IUuid {
    private UUID id;
}
