package hu.unimiskolc.iit.foci.server.engine;

import java.util.UUID;

public interface IGameLogic {
    void playGame(UUID gameId);
}
