package hu.unimiskolc.iit.foci.server.model;

import lombok.Data;

@Data
public class Player {
    private String id;
    private Position pos;
}
