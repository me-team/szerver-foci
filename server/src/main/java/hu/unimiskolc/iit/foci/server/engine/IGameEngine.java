package hu.unimiskolc.iit.foci.server.engine;

public interface IGameEngine extends Runnable {

    void init();

    void gameLoop();

    void update();

    void input();
}
