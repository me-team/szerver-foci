package hu.unimiskolc.iit.foci.server.comms.messages;

import lombok.Data;
import lombok.ToString;

import java.util.UUID;

@Data
@ToString
public class LoginMessage {
    private UUID clientId;
}
