package hu.unimiskolc.iit.foci.server.comms.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.UUID;

@NoArgsConstructor
//@RequiredArgsConstructor
@AllArgsConstructor
@Data
@ToString
public class GameMessage {
    /**
     * The ID should be the ID of the message received from Server.
     */
    private UUID id;
    private UUID clientId;
    /**
     * What game the client is playing.
     */
    private UUID gameStateId;
    private String type;
    private Object data;
}
