package hu.unimiskolc.iit.foci.server.persist.dao;

import hu.unimiskolc.iit.foci.server.model.GameState;
import hu.unimiskolc.iit.foci.server.persist.database.IFociDatabase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;

@Repository
public class GameStateDAO implements IDao<GameState> {

    private IFociDatabase database;

    @Autowired
    public void setDatabase(IFociDatabase database) {
        this.database = database;
    }

    @Override
    public void add(GameState gameState) {
        database.addGameState(gameState);
    }

    @Override
    public void update(GameState gameState) {
        database.updateGameState(gameState);
    }

    @Override
    public void delete(UUID id) {
        database.deleteGameState(id);
    }

    @Override
    public GameState get(UUID id) {
        return database.getGameState(id);
    }

    @Override
    public GameState getFirst(Predicate<? super GameState> predicate) {
        return database.getFirstGameState(predicate);
    }

    @Override
    public List<GameState> getAll() {
        return database.getAllGameStates();
    }

    @Override
    public List<GameState> getAllWithFilter(Predicate<? super GameState> predicate) {
        return database.getAllGameStatesWithFilter(predicate);
    }
}
