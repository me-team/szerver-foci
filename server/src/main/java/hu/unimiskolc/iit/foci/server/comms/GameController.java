package hu.unimiskolc.iit.foci.server.comms;

import hu.unimiskolc.iit.foci.server.comms.handler.IMessageHandler;
import hu.unimiskolc.iit.foci.server.comms.messages.GameMessage;
import hu.unimiskolc.iit.foci.server.comms.messages.GameOutputMessage;
import hu.unimiskolc.iit.foci.server.comms.messages.LoginMessage;
import hu.unimiskolc.iit.foci.server.comms.messages.LoginOutputMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class GameController {

    private IMessageHandler messageHandler;

    @Autowired
    public void setMessageHandler(IMessageHandler messageHandler) {
        this.messageHandler = messageHandler;
    }

    @MessageMapping("/game")
    @SendTo("/topic/game")
    public GameOutputMessage handleGame(final GameMessage message) throws Exception {
        return messageHandler.handleGame(message);
    }

    @MessageMapping("/login")
    @SendTo("/topic/game")
    public LoginOutputMessage handleLogin(final LoginMessage message) throws Exception {
        return messageHandler.handleLogin(message);
    }

}
