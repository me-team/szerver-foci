package hu.unimiskolc.iit.foci.server.persist.database;

import hu.unimiskolc.iit.foci.server.model.GameState;

import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;

public interface IGameStateDatabase {
    void addGameState(GameState model);

    void updateGameState(GameState model);

    void deleteGameState(UUID id);

    GameState getGameState(UUID id);

    GameState getFirstGameState(Predicate<? super GameState> predicate);

    List<GameState> getAllGameStates();

    List<GameState> getAllGameStatesWithFilter(Predicate<? super GameState> predicate);
}
