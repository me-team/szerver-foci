package hu.unimiskolc.iit.foci.server.persist.dao;

import hu.unimiskolc.iit.foci.server.model.Client;
import hu.unimiskolc.iit.foci.server.persist.database.IFociDatabase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;

@Repository
public class ClientDAO implements IDao<Client> {

    private IFociDatabase database;

    @Autowired
    public void setDatabase(IFociDatabase database) {
        this.database = database;
    }

    @Override
    public void add(Client client) {
        database.addClient(client);
    }

    @Override
    public void update(Client client) {
        database.updateClient(client);
    }

    @Override
    public void delete(UUID id) {
        database.deleteClient(id);
    }

    @Override
    public Client get(UUID id) {
        return database.getClient(id);
    }

    @Override
    public Client getFirst(Predicate<? super Client> predicate) {
        return database.getFirstClient(predicate);
    }

    @Override
    public List<Client> getAll() {
        return database.getAllClients();
    }

    @Override
    public List<Client> getAllWithFilter(Predicate<? super Client> predicate) {
        return database.getAllClientsWithFilter(predicate);
    }
}
