package hu.unimiskolc.iit.foci.server.model;

import hu.unimiskolc.iit.foci.server.comms.messages.GameMessage;
import hu.unimiskolc.iit.foci.server.comms.messages.OutputMessage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.UUID;

@NoArgsConstructor
//@RequiredArgsConstructor
@AllArgsConstructor
@Data
@Accessors(chain = true)
@ToString
public class Session implements IUuid {
    private UUID id;
    private UUID gameStateId;
    private OutputMessage outputMessage;
    private UUID client1Id;
    private UUID client2Id;
    private GameMessage client1response;
    private GameMessage client2response;

    public boolean bothClientsHaveInputs() {
        return client1response != null && client2response != null;
    }

    public boolean isOpenToJoin() {
        return (client1Id == null || client2Id == null) && (client1Id != client2Id);
    }
}
