package hu.unimiskolc.iit.foci.server.persist.database;

import hu.unimiskolc.iit.foci.server.model.Client;

import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;

public interface IClientDatabase {
    void addClient(Client model);

    void updateClient(Client model);

    void deleteClient(UUID id);

    Client getClient(UUID id);

    Client getFirstClient(Predicate<? super Client> predicate);

    List<Client> getAllClients();

    List<Client> getAllClientsWithFilter(Predicate<? super Client> predicate);
}
