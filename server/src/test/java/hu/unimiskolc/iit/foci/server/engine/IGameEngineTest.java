package hu.unimiskolc.iit.foci.server.engine;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class IGameEngineTest {

    @InjectMocks
    @Spy
    private IGameEngine gameEngine = new GameEngine();
    @Mock
    private IGameLogic gameLogic;

   /* @BeforeEach
    void beforeEach() {
        //MockitoAnnotations.initMocks(this);
    }

    @Test
    void testRun() {
        doThrow(new MockitoException("gameLoop() was called")).when(gameEngine).gameLoop();
        assertThrows(MockitoException.class, () -> {
            gameEngine.run();
        });
        verify(gameEngine).init();
    }

    @Test
    void testInit() {
        fail("not implemented yet");
    }

    @Test
    void testGameLoop() {
        fail("not implemented yet");
    }

    @Test
    void testUpdate() {
        fail("not implemented yet");
    }

    @Test
    void testInput() {
        fail("not implemented yet");
    }*/

}