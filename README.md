# Szerver foci

Java szerver foci
# Messages

    Login		    1Client:(messageType=CONNECT, clientId=<1ClientSajátId>,lineUp=[list<Player>])

    Login response 	    Server: (messageType=WAITING, clientId=<1ClientId>, id=<messageId>, gameStateId=<gamestate>)

    Login2		    2Client: (messageType=CONNECT, clientId=<2ClientSajátId>,lineUp=[list<Player>])

    Login response2     Server: (messageType=CONNECTED, clientId=<2ClientId>, id=<messageid>, gameStateId=<gamestate>,pitch=<Pitch>)
	
	Movement            Server: (messageType=STATUS, clientId=<clientId>, id=<messageid>, gameStateId=<gamestateId>,gameState=<gamestate/footballGame>)
	
	                    Client: (messageType=MOVEMENT, clientId=<clientId>, id=<messageId>,gamestateId=<gamestateId>, movements=<List Movement>)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With


## Contributing


## Versioning


## Authors

See also the list of [contributors](https://gitlab.com/me-team/szerver-foci/-/graphs/master) who participated in this project.

## License

This project is not licensed. Pls don't steal.

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
